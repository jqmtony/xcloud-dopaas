# XCloud DoPaaS UDS
> (Unified Distributed Scheduler) 统一分布式调度管控中心, 如 支持ElasticJob任务、spark/flink任务等

### Shardingsphere ElasticJob
- 当前对应官方开源仓库[shardingsphere-elasticjob(github)](https://github.com/apache/shardingsphere-elasticjob-ui)的开发提交commitId为: `6d9f6ee191728c18e4e52525af44d1699c4c0876`, 未来将基于此commitId进行同步新特性