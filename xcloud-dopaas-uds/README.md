# XCloud DoPaaS UDS
> (Unified Distributed Scheduler) support elasticjob/spark/flink task scheduling etc.

### Shardingsphere ElasticJob
- Currently corresponding to the official open source repository [shardingsphere-elasticjob(github)](https://github.com/apache/shardingsphere-elasticjob-ui) The development and commitId log are: `6d9f6ee191728c18e4e52525af44d1699c4c0876`, In the future, new features will be synchronized based on this commitId